﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationMitrais.Models;

namespace WebApplicationMitrais.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ViewResult Index(Registration reg)
        {
            string message = string.Empty;

            try
            {
                using (var ctx = new MitraisTestEntities())
                {
                    var mobileNu = ctx.Registration
                                    .Where(s => s.MobileNumber == reg.MobileNumber)
                                    .FirstOrDefault<Registration>();
                    if (mobileNu != null)
                    {
                        ViewBag.Message = -1;//"Mobile Number already exists.\\nPlease choose a different Mobile Number.";
                        return View("Index", reg);
                    }

                    var emailS = ctx.Registration
                                    .Where(s => s.Email == reg.Email)
                                    .FirstOrDefault<Registration>();
                    if (emailS != null)
                    {
                        ViewBag.Message = -2;// "Email address has already been used.";
                        return View("Index", reg);
                    }
                    ctx.Registration.Add(reg);

                    ctx.SaveChanges();

                    ViewBag.Message = reg.IDnya;
                    return View("Index", reg);



                }

                //ViewBag.Message = -1;
                //return View("Index", reg);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }




        }

        public ActionResult Login()
        {
            ViewBag.Message = "Your login page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}