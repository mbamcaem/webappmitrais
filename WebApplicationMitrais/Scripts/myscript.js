﻿//if no errors form will be submited  
$.validator.setDefaults({  
    submitHandler: function () {  
        alert("form submited!");  
    }  
});  
debugger;  
$(document).ready(function () {  
    $("#RegistrationForm").validate({  
        rules: {
            MobileNumber: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 12
            },
            FirstName: {  
                required: true,  
                minlength: 4,  
                maxlength: 15  
            },  
            LastName: {  
                required: true,  
                minlength: 4,  
                maxlength: 15  
            },                          
            Email: {  
                required: true,  
                email: true  
            }            
        },  
        messages: {
            MobileNumber: {
                required: "pls enter mobile no",
                minlength: "pls enter valid mobile no"
            },
            FirstName: {  
                required: "Firstname required Firstname required Firstname required Firstname required ",  
                minlength: "pls enter min 4 chars",  
                maxlength:"only 15 chars are allowed"  
            },  
            LastName: {  
                required: "Lastname required",  
                minlength: "pls enter min 4 chars",  
                maxlength: "only 15 chars are allowed"  
            },                       
            Email: {
                required: "Email required",
                minlength: "pls enter min 4 chars",
                maxlength: "only 15 chars are allowed"
            }
           

  
        },  
        //tooltip options to change position of tooltip..  
        tooltip_options: {  
            FirstName: { trigger: 'focus' },  
            Gender: { placement: 'top', html: true }    //placement:'bottom'for bottom  
        },  
         
        invalidHandler: function (form, validator) {  
            $("#validity_label").html('There be ' + validator.numberOfInvalids() + ' error' + (validator.numberOfInvalids() > 1 ? 's' : '') + ' here.  OH NOES!!!!!  ');  
        }  
});  
});  